//
//  Color1.swift
//  TabApp
//
//  Created by verec on 29/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

struct ColorProvider {
    static let beautyfulColor = UIColor(hue: 0.5, saturation: 0.7, brightness: 0.9, alpha: 0.3)
}


public class Color1 : NSObject {

    public var mainColor : UIColor {
        return ColorProvider.beautyfulColor
    }
}