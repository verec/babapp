//
//  SecondViewController.swift
//  TabApp
//
//  Created by verec on 29/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import UIKit

import TabAppWidget2

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        self.view.backgroundColor = Color2().mainColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

