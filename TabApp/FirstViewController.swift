//
//  FirstViewController.swift
//  TabApp
//
//  Created by verec on 29/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import UIKit

import TapAppWidget1
import TapAppWidget3

class FirstViewController: UIViewController {

    @IBOutlet weak var textLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        self.view.backgroundColor = Color1().mainColor

        let widget3:TapAppWidget3.Colors = TapAppWidget3.Colors()

        self.textLabel?.backgroundColor = widget3.mainColor

//        let text = widget3.

//        self.textLabel?.text = widget3.resourceTextLabel
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

