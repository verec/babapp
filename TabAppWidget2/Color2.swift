//
//  Color2.swift
//  TabApp
//
//  Created by verec on 29/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

struct ColorProvider {
    static let beautyfulColor = UIColor(hue: 0.7, saturation: 0.9, brightness: 0.3, alpha: 0.5)
}

public class Color2 : NSObject {

    public var mainColor : UIColor {
        return ColorProvider.beautyfulColor
    }
}